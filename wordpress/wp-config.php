<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '%?>a-+GEWiPOFN]q<)o9}o8$h g=7|/7GwV@=M>.g`pE?!GZ5czb$R]%eDD|f|!7' );
define( 'SECURE_AUTH_KEY',  'e@_=P!vY;of4N.m.JZxR9h0HN>XI~k7c+O]RZKP<zM-PH:Bq|cOPNxO {d{2,$w,' );
define( 'LOGGED_IN_KEY',    'eFpk?w)xrkBoUrAx+9~fW>Y|U1Z^d*@~p)8-C$,YG[IUrE39{hlD~orzi#8jmoGG' );
define( 'NONCE_KEY',        '@r^6Jbjq{%2tn@ZEfw21Dfm3U2Y.4-fnBxE;J8Jyr{ph)CpL<6?cw!a>Tm)!7: 8' );
define( 'AUTH_SALT',        'V?INlb5YzN6nAWqEf0Xl<=onc{:D^6lBu01L1tC7d]rE_OvnHO5E:C.awIHa4BXE' );
define( 'SECURE_AUTH_SALT', '2 4a#0+RnX|`:1#PMVH8Kfc>jb`7=uY<SVlt87.zaQ=+@pCR;_A%qX0{4d)RbJh9' );
define( 'LOGGED_IN_SALT',   '0Ejk)1/DCL]n~[b#otNV>dJ}}=BvK>JT:rz,-%5lCPW=E5,M)6<euMqnb/jXj7--' );
define( 'NONCE_SALT',       'HFMyL=D^%BS@oXJebbbvS@.4XC26b?5)XcIwz`oeHu5gpr p2{R8g(80tY:~lkt<' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'tb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
